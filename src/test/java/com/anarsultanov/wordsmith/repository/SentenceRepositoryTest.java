package com.anarsultanov.wordsmith.repository;

import com.anarsultanov.wordsmith.entity.Sentence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SentenceRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private SentenceRepository sentenceRepository;

    @Test
    public void saveSentence() {
        Sentence sentence = new Sentence();
        sentence.setContent("test");
        sentence.setReversed("tset");
        long savedSentenceId = sentenceRepository.save(sentence).getId();

        Sentence sentenceInDb = entityManager.find(Sentence.class, savedSentenceId);
        assertEquals(sentence.getContent(), sentenceInDb.getContent());
        assertEquals(sentence.getReversed(), sentenceInDb.getReversed());
    }

    @Test
    public void findLatestTenSentences() {
        for (int i = 1; i <= 12; i++) { // Save 12 sentences
            Sentence sentence = new Sentence();
            sentence.setContent(String.valueOf(i));
            entityManager.persistAndFlush(sentence);
        }

        List<Sentence> latest = sentenceRepository.findFirst10ByOrderByIdDesc();
        assertEquals(10, latest.size());
        assertEquals("12", latest.get(0).getContent()); // latest sentence should be first
        assertEquals("3", latest.get(9).getContent());
    }
}