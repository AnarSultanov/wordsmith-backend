package com.anarsultanov.wordsmith.controller;

import com.anarsultanov.wordsmith.entity.Sentence;
import com.anarsultanov.wordsmith.service.SentenceService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SentenceController.class)
public class SentenceControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SentenceService sentenceService;

    private static final Sentence sentence = new Sentence();

    @BeforeClass
    public static void setUp() {
        sentence.setContent("Hello, world!");
        sentence.setReversed("olleH, dlrow!");
    }

    @Test
    public void getRecent() throws Exception {
        when(sentenceService.getLatestTenSentences()).thenReturn(Collections.singletonList(sentence));

        MvcResult mvcResult = mvc.perform(get("/sentences")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        Sentence[] result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Sentence[].class);
        assertEquals(1, result.length);
        assertEquals(sentence, result[0]);
    }

    @Test
    public void process() throws Exception {
        when(sentenceService.process(sentence)).thenReturn(sentence);

        MvcResult mvcResult = mvc.perform(post("/sentences/process")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(sentence)))
                .andExpect(status().isOk())
                .andReturn();

        Sentence result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Sentence.class);
        assertEquals(sentence.getContent(), result.getContent());
        assertEquals(sentence.getReversed(), result.getReversed());
    }
}