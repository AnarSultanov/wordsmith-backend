package com.anarsultanov.wordsmith.service;

import com.anarsultanov.wordsmith.entity.Sentence;
import com.anarsultanov.wordsmith.repository.SentenceRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class SentenceServiceTest {

    @InjectMocks
    private SentenceService sentenceService;

    @Mock
    private SentenceRepository sentenceRepository;

    @Test
    public void getLatestTenSentences() {
        List<Sentence> sentenceList = new ArrayList<>();
        Mockito.when(sentenceRepository.findFirst10ByOrderByIdDesc()).thenReturn(sentenceList);

        assertEquals(sentenceList, sentenceService.getLatestTenSentences());
    }

    @Test
    public void processSentenceWithSpecialCharacterInTheWord() {
        testProcess("This's a test.", "s'sihT a tset.");
    }

    @Test
    public void processSentenceWithPunctuationMarkBetweenWords() {
        testProcess("Hello, world!", "olleH, dlrow!");
    }

    @Test
    public void processSentenceWithMultiplePunctuationMarksInTheEnd() {
        testProcess("He said what?!", "eH dias tahw?!");
    }

    private void testProcess(String content, String expectedResult) {
        Sentence sentence = new Sentence();
        sentence.setContent(content);
        sentenceService.process(sentence);
        verify(sentenceRepository, times(1)).save(sentence);
        assertEquals(expectedResult, sentence.getReversed());
    }
}