package com.anarsultanov.wordsmith.repository;

import com.anarsultanov.wordsmith.entity.Sentence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SentenceRepository extends CrudRepository<Sentence, Long> {
    List<Sentence> findFirst10ByOrderByIdDesc();
}
