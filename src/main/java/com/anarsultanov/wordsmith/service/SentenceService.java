package com.anarsultanov.wordsmith.service;

import com.anarsultanov.wordsmith.entity.Sentence;
import com.anarsultanov.wordsmith.repository.SentenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class SentenceService {

    private SentenceRepository sentenceRepository;

    @Autowired
    public SentenceService(SentenceRepository sentenceRepository) {
        this.sentenceRepository = sentenceRepository;
    }

    /**
     * Get the last ten processed sentences
     * @return list of sentences
     */
    public List<Sentence> getLatestTenSentences() {
        return sentenceRepository.findFirst10ByOrderByIdDesc();
    }

    /**
     * Process {@code sentence} by calling {@link #reverseWords(String)} on its content
     * and filling the corresponding field of the object with the result.
     * Populated object is stored in the database and returned to the caller.
     * @param sentence sentence to process
     * @return processed sentence
     */
    public Sentence process(Sentence sentence) {
        String reversed = reverseWords(sentence.getContent());
        sentence.setReversed(reversed);
        sentenceRepository.save(sentence);
        return sentence;
    }

    /**
     * Reverse all words in the sentence leaving them and punctuation marks in their places
     * e.g. "Hello, world!" and the expected result is "olleH, dlrow!"
     * @param content content to be processed
     * @return processed content
     */
    private static String reverseWords(String content) {
        String[] words = content.split(" ");
        List<String> reversedWords = new ArrayList<>(words.length);
        for (String word : words) {
            String reversedWord = reverseWord(word);
            reversedWords.add(reversedWord);
        }
        return StringUtils.arrayToDelimitedString(reversedWords.toArray(), " ");
    }

    /**
     * Suppose that after the sentence is divided into words, special characters can occur:
     * 1. One character in the middle of the word (e.g. "It's" and the expected result is "s'tI")
     * 2. One or more characters at the end of the word (e.g. "what?!" and the expected result is "tahw?!")
     * All other combinations of letters and symbols obtained as a result of an incorrectly formulated sentence
     * can lead to unpredictable results.
     * @param word the word to reverse
     * @return reversed word
     */
    private static String reverseWord(String word) {
        char[] chars = word.toCharArray();
        StringBuilder reversed = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (Character.isLetterOrDigit(ch)) {
                reversed.insert(0, ch);
            } else if (i == chars.length - 1) {
                reversed.append(ch);
            } else if (Character.isLetterOrDigit(chars[i + 1])) {
                reversed.insert(0, ch);
            } else {
                reversed.append(ch);
            }
        }
        return reversed.toString();
    }
}
