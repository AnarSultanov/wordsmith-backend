package com.anarsultanov.wordsmith.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "sentence")
public class Sentence {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    @Column(name = "content")
    private String content;

    @Column(name = "reversed")
    private String reversed;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReversed() {
        return reversed;
    }

    public void setReversed(String reversed) {
        this.reversed = reversed;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Sentence sentence = (Sentence) o;
        return id == sentence.id &&
                Objects.equals(content, sentence.content) &&
                Objects.equals(reversed, sentence.reversed);
    }

    @Override public int hashCode() {
        return Objects.hash(id, content, reversed);
    }

    @Override public String toString() {
        return "Sentence{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", reversed='" + reversed + '\'' +
                '}';
    }
}
