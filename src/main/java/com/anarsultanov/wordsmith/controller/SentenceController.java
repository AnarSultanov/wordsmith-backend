package com.anarsultanov.wordsmith.controller;

import com.anarsultanov.wordsmith.entity.Sentence;
import com.anarsultanov.wordsmith.service.SentenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/sentences")
public class SentenceController {

    private SentenceService sentenceService;

    @Autowired
    public SentenceController(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    @GetMapping
    public List<Sentence> getRecent() {
        return sentenceService.getLatestTenSentences();
    }

    @PostMapping("/process")
    public Sentence process(@RequestBody Sentence sentence) {
        return sentenceService.process(sentence);
    }
}
