# WordsmithBackend

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

All commands further use the [maven wrapper](https://github.com/takari/maven-wrapper).
If you want to use the regular `mvn` command, you will need
[Maven v3.2.1 or above](https://maven.apache.org/run-maven/index.html).

## Development

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.anarsultanov.wordsmith.WordsmithApplication` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

----
	$ ./mvnw spring-boot:run
----

By default, the application will use H2 in memory database.
For more information about setting up a more suitable database for you, you can read:

- [Working with SQL Databases](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-sql.html)
- [Working with NoSQL Technologies](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-nosql.html)

## Test

To launch tests, run:

----
	$ ./mvnw clean test
----

## Build

To build an executable jar, run:

----
	$ ./mvnw clean package
----

